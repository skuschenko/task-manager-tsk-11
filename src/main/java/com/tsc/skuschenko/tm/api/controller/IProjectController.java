package com.tsc.skuschenko.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void create();

    void clear();

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

}

package com.tsc.skuschenko.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void create();

    void clear();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByName();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void updateTaskById();

    void updateTaskByIndex();

}

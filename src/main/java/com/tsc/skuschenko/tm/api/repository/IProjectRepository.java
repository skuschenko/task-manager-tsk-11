package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void add(Project Project);

    void remove(Project Project);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

}

package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(String name, String description);

    void add(Project task);

    void remove(Project task);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project updateOneById(String id, String name, String description);

    Project updateOneByIndex(Integer index, String name, String description);

}

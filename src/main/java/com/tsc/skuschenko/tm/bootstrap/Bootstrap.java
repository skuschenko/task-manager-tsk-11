package com.tsc.skuschenko.tm.bootstrap;

import com.tsc.skuschenko.tm.api.controller.ICommandController;
import com.tsc.skuschenko.tm.api.controller.IProjectController;
import com.tsc.skuschenko.tm.api.controller.ITaskController;
import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.service.ICommandService;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.controller.CommandController;
import com.tsc.skuschenko.tm.controller.ProjectController;
import com.tsc.skuschenko.tm.controller.TaskController;
import com.tsc.skuschenko.tm.repository.CommandRepository;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import com.tsc.skuschenko.tm.service.CommandService;
import com.tsc.skuschenko.tm.service.ProjectService;
import com.tsc.skuschenko.tm.service.TaskService;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository
            = new CommandRepository();

    private final ICommandService commandService
            = new CommandService(commandRepository);

    private final ICommandController commandController
            = new CommandController(commandService);

    private final ITaskRepository taskRepository
            = new TaskRepository();

    private final ITaskService taskService
            = new TaskService(taskRepository);

    private final ITaskController taskController
            = new TaskController(taskService);

    private final IProjectRepository projectRepository
            = new ProjectRepository();

    private final IProjectService projectService
            = new ProjectService(projectRepository);

    private final IProjectController projectController
            = new ProjectController(projectService);

    public void run(final String... args) {
        System.out.println("***Welcome to task manager***");
        if (parseArgs(args)) commandController.exit();
        while (true) {
            System.out.print("Enter command:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg.toLowerCase()) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showWrongArg(arg);
                break;
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command.toLowerCase()) {
            case TerminalConst.TM_ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.TM_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.TM_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.TM_EXIT:
                commandController.exit();
                break;
            case TerminalConst.TM_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.TM_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.TM_ARGS:
                commandController.showArgs();
                break;
            case TerminalConst.TM_TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TM_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.TM_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.TM_TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TM_TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TM_TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TM_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TM_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TM_TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.TM_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TM_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TM_PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.TM_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.TM_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.TM_PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.TM_PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.TM_PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.TM_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.TM_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.TM_PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.TM_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.TM_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            default:
                commandController.showWrongCmd(command);
                break;
        }
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}

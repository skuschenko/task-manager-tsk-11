package com.tsc.skuschenko.tm.constant;

public interface TerminalConst {

    String TM_VERSION = "version";

    String TM_HELP = "help";

    String TM_ABOUT = "about";

    String TM_EXIT = "exit";

    String TM_INFO = "info";

    String TM_TASK_CREATE = "task-create";

    String TM_TASK_CLEAR = "task-clear";

    String TM_TASK_LIST = "task-list";

    String TM_TASK_VIEW_BY_ID = "task-view-by-id";

    String TM_TASK_VIEW_BY_INDEX = "task-view-by-index";

    String TM_TASK_VIEW_BY_NAME = "task-view-by-name";

    String TM_TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TM_TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String TM_TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String TM_TASK_UPDATE_BY_ID = "task-update-by-id";

    String TM_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TM_PROJECT_VIEW_BY_ID = "project-view-by-id";

    String TM_PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String TM_PROJECT_VIEW_BY_NAME = "project-view-by-name";

    String TM_PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String TM_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String TM_PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String TM_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String TM_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String TM_PROJECT_CREATE = "project-create";

    String TM_PROJECT_CLEAR = "project-clear";

    String TM_PROJECT_LIST = "project-list";

    String TM_COMMANDS = "commands";

    String TM_ARGS = "arguments";

}

package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.controller.IProjectController;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        final String showProjectInfo = "["
                + TerminalConst.TM_PROJECT_LIST.toUpperCase()
                + "]";
        System.out.println(showProjectInfo);
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

    public void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void create() {
        final String methodInfo = "["
                + TerminalConst.TM_PROJECT_CREATE.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void clear() {
        final String clearInfo = "["
                + TerminalConst.TM_PROJECT_CLEAR.toUpperCase()
                + "]";
        System.out.println(clearInfo);
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showProjectById() {
        final String methodInfo = "["
                + TerminalConst.TM_PROJECT_VIEW_BY_ID.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER ID:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(value);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        final String methodInfo = "["
                + TerminalConst.TM_PROJECT_VIEW_BY_INDEX.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER INDEX:");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(value);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByName() {
        final String methodInfo = "["
                + TerminalConst.TM_PROJECT_VIEW_BY_NAME.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER NAME:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(value);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        final String methodInfo = "["
                + TerminalConst.TM_PROJECT_REMOVE_BY_ID.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER ID:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(value);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        final String methodInfo = "["
                + TerminalConst.TM_PROJECT_REMOVE_BY_INDEX.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER INDEX:");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(value);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        final String methodInfo = "["
                + TerminalConst.TM_PROJECT_REMOVE_BY_NAME.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER NAME:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(value);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        final String methodInfo = "["
                + TerminalConst.TM_PROJECT_UPDATE_BY_ID.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER ID:");
        final String valueId = TerminalUtil.nextLine();
        Project project = projectService.findOneById(valueId);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String valueName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String valueDescription = TerminalUtil.nextLine();
        project = projectService.updateOneById(
                valueId,
                valueName,
                valueDescription);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        final String methodInfo = "["
                + TerminalConst.TM_PROJECT_UPDATE_BY_INDEX.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER INDEX:");
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        Project project = projectService.findOneByIndex(valueIndex);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String valueName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String valueDescription = TerminalUtil.nextLine();
        project = projectService.updateOneByIndex(
                valueIndex,
                valueName,
                valueDescription);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

}
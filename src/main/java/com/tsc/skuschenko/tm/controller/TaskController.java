package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.controller.ITaskController;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        final String showListInfo = "["
                + TerminalConst.TM_TASK_LIST.toUpperCase()
                + "]";
        System.out.println(showListInfo);
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    public void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void create() {
        final String methodInfo = "["
                + TerminalConst.TM_TASK_CREATE.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void clear() {
        final String clearInfo = "["
                + TerminalConst.TM_TASK_CLEAR.toUpperCase()
                + "]";
        System.out.println(clearInfo);
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
        final String methodInfo = "["
                + TerminalConst.TM_TASK_VIEW_BY_ID.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER ID:");
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(value);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        final String methodInfo = "["
                + TerminalConst.TM_TASK_VIEW_BY_INDEX.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER INDEX:");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(value);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByName() {
        final String methodInfo = "["
                + TerminalConst.TM_TASK_VIEW_BY_NAME.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER NAME:");
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(value);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        final String methodInfo = "["
                + TerminalConst.TM_TASK_REMOVE_BY_ID.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER ID:");
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(value);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        final String methodInfo = "["
                + TerminalConst.TM_TASK_REMOVE_BY_INDEX.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER INDEX:");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeOneByIndex(value);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        final String methodInfo = "["
                + TerminalConst.TM_TASK_REMOVE_BY_NAME.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER NAME:");
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(value);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        final String methodInfo = "["
                + TerminalConst.TM_TASK_UPDATE_BY_ID.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER ID:");
        final String valueId = TerminalUtil.nextLine();
        Task task = taskService.findOneById(valueId);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String valueName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String valueDescription = TerminalUtil.nextLine();
        task = taskService.updateOneById(
                valueId,
                valueName,
                valueDescription);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        final String methodInfo = "["
                + TerminalConst.TM_TASK_UPDATE_BY_INDEX.toUpperCase()
                + "]";
        System.out.println(methodInfo);
        System.out.println("ENTER INDEX:");
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        Task task = taskService.findOneByIndex(valueIndex);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String valueName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String valueDescription = TerminalUtil.nextLine();
        task = taskService.updateOneByIndex(
                valueIndex,
                valueName,
                valueDescription);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

}
package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.TM_ABOUT,
            ArgumentConst.ARG_ABOUT,
            "show about information"
    );

    private static final Command HELP = new Command(
            TerminalConst.TM_HELP,
            ArgumentConst.ARG_HELP,
            "show help information"
    );

    private static final Command VERSION = new Command(
            TerminalConst.TM_VERSION,
            ArgumentConst.ARG_VERSION,
            "show version application"
    );

    private static final Command EXIT = new Command(
            TerminalConst.TM_EXIT,
            null,
            "close program"
    );

    private static final Command INFO = new Command(
            TerminalConst.TM_INFO,
            ArgumentConst.ARG_INFO,
            "close program"
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.TM_ARGS,
            null,
            "show arguments"
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.TM_COMMANDS,
            null,
            "show commands"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TM_TASK_CREATE,
            null,
            "create new task"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TM_TASK_CLEAR,
            null,
            "clear all tasks"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TM_TASK_LIST,
            null,
            "show all tasks"
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.TM_TASK_VIEW_BY_ID,
            null,
            "find task by id"
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.TM_TASK_VIEW_BY_INDEX,
            null,
            "find task by index"
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.TM_TASK_VIEW_BY_NAME,
            null,
            "find task by name"
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TM_TASK_REMOVE_BY_ID,
            null,
            "remove task by id"
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TM_TASK_REMOVE_BY_INDEX,
            null,
            "remove task by index"
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TM_TASK_REMOVE_BY_NAME,
            null,
            "remove task by name"
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TM_TASK_UPDATE_BY_ID,
            null,
            "update task by id"
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TM_TASK_UPDATE_BY_INDEX,
            null,
            "update task by index"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.TM_PROJECT_CREATE,
            null,
            "create new project"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.TM_PROJECT_CLEAR,
            null,
            "clear all projects"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.TM_PROJECT_LIST,
            null,
            "show all projects"
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.TM_PROJECT_VIEW_BY_ID,
            null,
            "find project by id"
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.TM_PROJECT_VIEW_BY_INDEX,
            null,
            "find project by index"
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.TM_PROJECT_VIEW_BY_NAME,
            null,
            "find project by name"
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.TM_PROJECT_REMOVE_BY_ID,
            null,
            "remove project by id"
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.TM_PROJECT_REMOVE_BY_INDEX,
            null,
            "remove project by index"
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.TM_PROJECT_REMOVE_BY_NAME,
            null,
            "remove project by name"
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.TM_PROJECT_UPDATE_BY_ID,
            null,
            "update project by id"
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.TM_PROJECT_UPDATE_BY_INDEX,
            null,
            "update project by index"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS, TASK_CREATE,
            TASK_CLEAR, TASK_LIST, TASK_VIEW_BY_ID, TASK_VIEW_BY_INDEX,
            TASK_VIEW_BY_NAME, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_REMOVE_BY_NAME, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            PROJECT_VIEW_BY_ID, PROJECT_VIEW_BY_INDEX, PROJECT_VIEW_BY_NAME,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_REMOVE_BY_NAME, PROJECT_UPDATE_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_LIST, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
